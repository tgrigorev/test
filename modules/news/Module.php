<?php

namespace app\modules\news;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\news\controllers';
    public $defaultRoute = 'news';

    public function init()
    {
        // custom initialization code goes here
    }
}
