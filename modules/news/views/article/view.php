<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\MNews */

$this->title = $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Mnews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mnews-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <?= Html::encode($model->Title); ?>
    </div>
    <div>
        <?= Html::encode($model->Description); ?>
    </div>
    <div>
        <?= Html::encode($model->theme->ThemeTitle); ?>
    </div>
    <div>
        <?= Html::encode($model->Text); ?>
    </div>
    <div>
        <?= Html::encode($model->Date); ?>
    </div>

</div>
