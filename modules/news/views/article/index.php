<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\models\MNewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mnews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mnews-index">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item'
    ]); ?>

</div>
