<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\MNews */

?>
<div class="news">
    <div><?= Html::encode($model->Title); ?></div>
    <div><?= Html::encode($model->Description); ?></div>
    <?= Html::a('читать далее',['view','id'=>$model->NewsId])?>
</div>