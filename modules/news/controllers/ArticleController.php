<?php

namespace app\modules\news\controllers;

use Yii;
use app\modules\news\models\MNews;
use app\modules\news\models\MNewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsController implements the CRUD actions for MNews model.
 */
class ArticleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MNews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MNewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 5);

        $aSecondUserMenu = new MNews();
        Yii::$app->view->params['secondMenu'] = $aSecondUserMenu->secondUserMenu();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    /**
     * Lists all MNews models.
     * @return mixed
     */
    public function actionPart()
    {
        $searchModel = new MNewsSearch();
        $dataProvider = $searchModel->searchPart(Yii::$app->request->queryParams, $_GET['type'],
            $_GET['param'], $_GET['theme'], 5);

        $aSecondUserMenu = new MNews();
        Yii::$app->view->params['secondMenu'] = $aSecondUserMenu->secondPartMenu($_GET['param']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MNews model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the MNews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MNews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MNews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
