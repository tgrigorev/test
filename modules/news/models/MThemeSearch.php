<?php

namespace app\modules\news\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\news\models\MThemes;

/**
 * MThemeSearch represents the model behind the search form about `app\modules\news\models\MThemes`.
 */
class MThemeSearch extends MThemes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ThemeId'], 'integer'],
            [['ThemeTitle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MThemes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ThemeId' => $this->ThemeId,
        ]);

        $query->andFilterWhere(['like', 'ThemeTitle', $this->ThemeTitle]);

        return $dataProvider;
    }
}
