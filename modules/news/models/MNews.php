<?php

namespace app\modules\news\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $NewsId
 * @property integer $ThemeId
 * @property string $Title
 * @property string $Description
 * @property string $Text
 * @property string $Date
 *
 * @property MThemes $theme
 */
class MNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ThemeId', 'Title','Description', 'Text', 'Date'], 'required'],
            [['ThemeId'], 'integer'],
            [['Description'], 'string', 'max' => 255],
            [['Text'], 'string'],
            [['Date'], 'safe'],
            [['Title'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NewsId' => 'News ID',
            'ThemeId' => 'Тема',
            'Title' => 'Название',
            'Description' => 'Описание',
            'Text' => 'Текст',
            'Date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(MThemes::className(), ['ThemeId' => 'ThemeId']);
    }

    public function secondUserMenu()
    {
        $sSql = 'SELECT YEAR(Date) year, MONTH(Date) month, COUNT(Date) `count` FROM ' . self::tableName() .
            ' GROUP BY YEAR(Date),MONTH(Date)';
        $oaMenu = self::findBySql($sSql)->asArray()->all();

        return $this->constructMenu($oaMenu);
    }

    public function secondPartMenu($sParam = NULL)
    {
        $oaSubMenu = NULL;
        $sSql = 'SELECT YEAR(`Date`) `year`, MONTH(`Date`) `month`, COUNT(`Date`) `count` FROM ' . self::tableName() .
            ' GROUP BY YEAR(`Date`),MONTH(`Date`)';
        $oaMenu = self::findBySql($sSql)->asArray()->all();

        if($sParam){
            $sSql = 'SELECT CONCAT(`t`.`ThemeTitle`, " (", COUNT(`t`.`ThemeId`), ")") `name`, '.
                '`t`.`ThemeId`, DATE_FORMAT(`n`.`Date`,"%Y-%c") `param` FROM ' . self::tableName() .
                ' `n` JOIN `themes` `t` ON `n`.`ThemeId` = `t`.`ThemeId` WHERE DATE_FORMAT(`n`.`Date`,"%Y-%c") = "'.
                $sParam.'" GROUP BY `n`.`ThemeId`';
            $oaSubMenu = self::findBySql($sSql)->asArray()->all();
        }

        return $this->constructMenu($oaMenu, $oaSubMenu, $sParam);
    }

    private function constructMenu($oaMenu, $oaSubMenu = NULL, $sParam = NULL){
        $aSecondMenu = [];
        $iYear = 0;

        foreach($oaMenu as $aMenu){
            if($aMenu['year'] != $iYear){
                $iYear = $aMenu['year'];
                $aSecondMenu[] = ['label' => $iYear, 'url' => ['/news/article/part/year/'.$iYear]];
            }
            $aSecondMenu[] = ['label' => $this->month()[$aMenu['month']].' ('.$aMenu['count'].')',
                'url' => ['/news/article/part/month/'.$aMenu['year'].'-'.$aMenu['month']],
                'options'=>['class'=>'submenu']];
            if($oaSubMenu && ($aMenu['year'].'-'.$aMenu['month']) == $sParam){
                foreach($oaSubMenu as $aSubMenu){
                    $aSecondMenu[] = ['label' => $aSubMenu['name'],
                        'url' => ['/news/article/part/month/'.$aSubMenu['param'].'/'.
                            $aSubMenu['ThemeId']],
                        'options'=>['class'=>'subthememenu']];
                }
            }
        }
        return $aSecondMenu;
    }

    private function month(){

        return ['Январь','Март','Апрель','Май','Июнь','Июль','Август',
            'Сентябрь','Октябрь','Ноябрь','Декабрь'];
    }
}
