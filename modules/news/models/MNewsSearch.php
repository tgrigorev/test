<?php

namespace app\modules\news\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\news\models\MNews;

/**
 * MNewsSearch represents the model behind the search form about `app\modules\news\models\MNews`.
 */
class MNewsSearch extends MNews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NewsId', 'ThemeId'], 'integer'],
            [['Title', 'Description', 'Text', 'Date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $pagination
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pagination = 20)
    {
        $query = MNews::find();

        $dataProvider = $this->searchMain($params, $pagination, $query);

        return $dataProvider;
    }

    public function searchPart($params, $type, $param, $theme, $pagination = 20)
    {
        if($type == 'year'){
            $where = 'YEAR(Date) = '.$param;
        }elseif(!empty($theme)){
            $where = 'DATE_FORMAT(Date,"%Y-%c") = "'.$param.'" AND `ThemeId` = '.$theme;
        }else{
            $where = 'DATE_FORMAT(Date,"%Y-%c") = "'.$param.'"';
        }

        $sSql = 'SELECT * FROM '.MNews::tableName().' WHERE '.$where;

        $query = MNews::findBySql($sSql);

        $dataProvider = $this->searchMain($params, $pagination, $query);

        return $dataProvider;
    }

    private function searchMain($params, $pagination, $query){
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize'=>$pagination]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'NewsId' => $this->NewsId,
            'ThemeId' => $this->ThemeId,
            'Date' => $this->Date,
        ]);

        $query->andFilterWhere(['like', 'Title', $this->Title])
            ->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'Text', $this->Text]);

        return $dataProvider;
    }

}
