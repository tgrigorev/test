<?php

namespace app\modules\news\models;

use Yii;

/**
 * This is the model class for table "themes".
 *
 * @property integer $ThemeId
 * @property string $ThemeTitle
 *
 * @property MNews[] $news
 */
class MThemes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ThemeTitle'], 'required'],
            [['ThemeTitle'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ThemeId' => 'Theme ID',
            'ThemeTitle' => 'Тема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(MNews::className(), ['ThemeId' => 'ThemeId']);
    }
}
