<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\MThemes */

$this->title = 'Update Mthemes: ' . ' ' . $model->ThemeId;
$this->params['breadcrumbs'][] = ['label' => 'Mthemes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ThemeId, 'url' => ['view', 'id' => $model->ThemeId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mthemes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
