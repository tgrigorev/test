<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\models\MThemeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mthemes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mthemes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mthemes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ThemeId',
            'ThemeTitle',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
