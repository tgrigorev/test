<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\models\MNewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mnews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mnews-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mnews', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'=>'ThemeId',
                'format'=>'raw',
                'value'=>function($data){
                        return $data->theme->ThemeTitle;
                    }
            ],
            'Title',
            'Description',
            'Text:text',
            'Date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
