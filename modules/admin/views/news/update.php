<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\MNews */

$this->title = 'Update Mnews: ' . ' ' . $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Mnews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Title, 'url' => ['view', 'id' => $model->NewsId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mnews-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
