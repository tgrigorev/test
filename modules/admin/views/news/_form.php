<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\MNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mnews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ThemeId')->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\modules\news\models\MThemes::find()->asArray()->all(),'ThemeId','ThemeTitle'));
        //->textInput() ?>

    <?= $form->field($model, 'Title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Date')->widget(\yii\jui\DatePicker::className()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
