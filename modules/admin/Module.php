<?php

namespace app\modules\admin;

use \yii\base\Theme;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';
    public $defaultRoute = 'news';

    public function init()
    {
        parent::init();

        parent::init();
        \Yii::$app->view->theme = new Theme([
            'pathMap' => ['@app/views' => '@app/modules/admin/views'],
            'baseUrl' => '@web/modules/admin',
        ]);
        // custom initialization code goes here
    }
}
